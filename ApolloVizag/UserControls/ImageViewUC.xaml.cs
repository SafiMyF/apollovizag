﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloVizag.UserControls
{
    /// <summary>
    /// Interaction logic for ImageViewUC.xaml
    /// </summary>
    public partial class ImageViewUC : UserControl
    {
        public ImageViewUC()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseImageViewUc;
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            EvntCloseImageViewUc(this, null);
        }
    }
}
