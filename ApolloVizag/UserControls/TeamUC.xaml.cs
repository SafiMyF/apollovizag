﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using ApolloVizag.Model;


namespace ApolloVizag.UserControls
{
    /// <summary>
    /// Interaction logic for TeamUC.xaml
    /// </summary>
    public partial class TeamUC : UserControl
    {
        public event EventHandler EvntCloseTeamUc;
        public TeamUC()
        {
            InitializeComponent();
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseTeamUc(this, null);
        }

        private void svDepartments_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnDepartmentItem_TouchDown(object sender, TouchEventArgs e)
        {
            GrdDeptMiniPanel.Visibility = Visibility.Visible;
            GrdDepartmentsPanel.Visibility = Visibility.Collapsed;
            GrdTeamPanel.Visibility = Visibility.Visible;
        }

        private void svDeptMiniList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            string deptFolder = ConfigurationManager.AppSettings["DepartmentPath"];
            string path = System.IO.Path.Combine(Apps.strPath, deptFolder);
            if (File.Exists(path))
            {

                string strInfo = System.IO.File.ReadAllText(path);
                Departments dept = Utilities.GetDepartments(strInfo);
                ICtrlDepartments.ItemsSource = dept.Department;
                ICtrlDepartmentsMiniList.ItemsSource = dept.Department;
            }
            else
            {

            }
        }
    }
}
