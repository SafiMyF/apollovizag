﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;

namespace ApolloVizag.UserControls
{
    /// <summary>
    /// Interaction logic for ContactUC.xaml
    /// </summary>
    public partial class ContactUC : UserControl
    {
        public event EventHandler EvntCloseContactUc;
        public ContactUC()
        {
            InitializeComponent();
        }
        private void CreatePushPins()
        {
            //17.717085, 83.309213
            double Latitude = 17.717085;
            double Longitude = 83.309213;
            Pushpin pin = CreatePushPin(Latitude, Longitude);
            if (pin != null)
            {
                map.Children.Add(pin);
            }
        }

        private Pushpin CreatePushPin(double Latitude, double Longitude)
        {
            if (Latitude != null && Longitude != null)
            {
                Pushpin pin = new Pushpin();
                pin.Location = new Location(Convert.ToDouble(Latitude), Convert.ToDouble(Longitude));
                return pin;
            }
            return null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            CreatePushPins();
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseContactUc(this, null);
        }

        private void svContactDetails_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
