﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloVizag.UserControls
{
    /// <summary>
    /// Interaction logic for VideoViewUC.xaml
    /// </summary>
    public partial class VideoViewUC : UserControl
    {
        public VideoViewUC()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseVideoUc;

        private void btnPlay_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Play();
            btnPause.Visibility = Visibility.Visible;
            btnPlay.Visibility = Visibility.Collapsed;
        }

        private void btnPause_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void btnStop_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Stop();
            MEVideo.Position = new TimeSpan(0, 0, 2);
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            EvntCloseVideoUc(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            MEVideo.Source = new Uri("C:/ApolloVizag/Media/video.mp4", UriKind.Relative);
            MEVideo.Play();
        }
    }
}
