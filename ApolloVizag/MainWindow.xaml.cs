﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloVizag.UserControls;

namespace ApolloVizag
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static int zIndexValue = 9999;
        public static int ivRenTrsf;
        public MainWindow()
        {
            InitializeComponent();
            ivRenTrsf = 10;
        }

        private int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }
        private void CheckRenderTransform()
        {
            int countIV = 0;
            foreach (Control ctrl in GrdUserControlPanel.Children)
            {
                if (ctrl is ImageViewUC || ctrl is VideoViewUC || ctrl is BroucherUC)
                    countIV++;
            }
            if (ivRenTrsf > 120 || countIV == 0)
                ivRenTrsf = 20;
            else
            {
                ivRenTrsf += 20;
            }
        }

        #region ManipulationEvents

        ManipulationModes currentMode = ManipulationModes.All;
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadHomePage();
        }

        private void LoadHomePage()
        {
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion
            GrdSlideshowPanel.Children.Clear();
            GrdMainLayoutPanel.Visibility = Visibility.Collapsed;
            LoadSlideShowUC();
        }

        private void LoadSlideShowUC()
        {
            SlideshowUC slideshow = new SlideshowUC();
            GrdSlideshowPanel.Children.Add(slideshow);
            //raise evenets from slideshow uc
            slideshow.EvntCloseSlideshowUc += slideshow_EvntCloseSlideshowUc;
            slideshow.EvntOpenContactUc += slideshow_EvntOpenContactUc;
        }

        void slideshow_EvntCloseSlideshowUc(object sender, EventArgs e)
        {
            SlideshowUC slideshow = (SlideshowUC)sender;
            GrdSlideshowPanel.Children.Remove(slideshow);
            LoadMenuPage();
        }

        private void LoadMenuPage()
        {
            GrdMainLayoutPanel.Visibility = Visibility.Visible;

        }

        void slideshow_EvntOpenContactUc(object sender, EventArgs e)
        {
            LoadContactUC();
        }

        private void LoadContactUC()
        {
            GrdLargeControlPanel.Children.Clear();
            ContactUC contact = new ContactUC();
            GrdLargeControlPanel.Children.Add(contact);
            contact.EvntCloseContactUc += contact_EvntCloseContactUc;
        }

        void contact_EvntCloseContactUc(object sender, EventArgs e)
        {
            ContactUC contact = (ContactUC)sender;
            GrdLargeControlPanel.Children.Remove(contact);
        }

        private void btnTeam_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadTeamUC();
        }

        private void LoadTeamUC()
        {
            TeamUC team = new TeamUC();
            GrdLargeControlPanel.Children.Add(team);
            //raise events from team
            team.EvntCloseTeamUc += team_EvntCloseTeamUc;
        }

        void team_EvntCloseTeamUc(object sender, EventArgs e)
        {
            TeamUC team = (TeamUC)sender;
            GrdLargeControlPanel.Children.Remove(team);
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadContactUC();
        }

        private void btnEmergency_PreviewTouchUp(object sender, TouchEventArgs e)
        {

        }

        private void svCategoryList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCateogryItem_TouchDown(object sender, TouchEventArgs e)
        {

        }

        #region VideoView
        private void LoadVideoViewUC()
        {
            CheckRenderTransform();
            VideoViewUC video = new VideoViewUC();
            video.IsManipulationEnabled = true;
            video.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
            video.BringIntoView();
            Panel.SetZIndex(video, SetZIndexValue());
            GrdUserControlPanel.Children.Add(video);
            video.EvntCloseVideoUc += video_EvntCloseVideoUc;
        }

        void video_EvntCloseVideoUc(object sender, EventArgs e)
        {
            VideoViewUC video = (VideoViewUC)sender;
            GrdUserControlPanel.Children.Remove(video);
        }
        #endregion

        #region ImageView
        private void LoadImageViewUC()
        {
            CheckRenderTransform();
            ImageViewUC image = new ImageViewUC();
            image.IsManipulationEnabled = true;
            image.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
            image.BringIntoView();
            Panel.SetZIndex(image, SetZIndexValue());
            GrdUserControlPanel.Children.Add(image);
            image.EvntCloseImageViewUc += image_EvntCloseImageViewUc;

        }

        void image_EvntCloseImageViewUc(object sender, EventArgs e)
        {
            ImageViewUC image = (ImageViewUC)sender;
            GrdUserControlPanel.Children.Remove(image);            
        }
        #endregion
        #region BroucherView
        private void LoadBroucherUC()
        {
            CheckRenderTransform();
            BroucherUC broucher = new BroucherUC();
            broucher.IsManipulationEnabled = true;
            broucher.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
            broucher.BringIntoView();
            Panel.SetZIndex(broucher, SetZIndexValue());
            GrdUserControlPanel.Children.Add(broucher);
        }

        #endregion
    }
}
