﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloVizag.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : UserControl
    {
        public event EventHandler EvntCloseAdminSetting;
        public event EventHandler EvntLoginAdmin;
        public event EventHandler EvntWarningAlert;
        public AdminLogin()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtName.Focus();
        }

        private void txtPassword_TouchEnter(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtName_TouchEnter(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }
        private void btnLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            ValidateLogin();
        }

        private void ValidateLogin()
        {
            CloseTabTip();
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtPassword.Password))
            {
                if (txtName.Text.Equals("admin") && txtPassword.Password.Equals("123"))
                {
                    EvntLoginAdmin(this, null);
                }
                else
                {
                    ClearFields();
                    EvntWarningAlert("Invalid username and password.", null);
                }
                //if (Apps.PingCheck())
                //{
                //    if (Apps.ValidateAdmin(txtName.Text, txtPassword.Password))
                //    {
                //        SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
                //        EvntLoginAdmin(this, null);
                //    }
                //    else
                //    {
                //        EvntWarningAlert("Invalid username and password.", null);
                //        //MessageBox.Show("Invalid username and password.", "Apollo", MessageBoxButton.OK);
                //        ClearFields();
                //    }
                //}
                //else if (txtName.Text.Trim().Equals(Utilities.UserName) && txtPassword.Password.Trim().Equals(Utilities.Password))
                //{
                //    EvntLoginAdmin(this, null);
                //}
                //else
                //{
                //    ClearFields();
                //    EvntWarningAlert("Invalid username and password.", null);
                //    //MessageBox.Show("Invalid username and password.", "Apollo", MessageBoxButton.OK);
                //}
            }
            else
            {
                ClearFields();
                EvntWarningAlert("Username and Password should not be empty.", null);
                //MessageBox.Show("Username and Password should not be empty.", "Apollo", MessageBoxButton.OK);
            }
        }

        private void ClearFields()
        {
            txtName.Text = string.Empty;
            txtPassword.Password = string.Empty;
            txtName.Focus();
        }

        private void SaveCredentials(string userName, string password)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["UserName"].Value = userName;
                config.AppSettings.Settings["Password"].Value = password;
                config.Save(ConfigurationSaveMode.Full, true);
            }
            catch (Exception)
            {
                //Log Here
            }
        }

        private void btnCanelLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            CloseTabTip();
            EvntCloseAdminSetting(this, null);
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        string processName;
        Process proc;
        private void OpenVirtualKeyboard()
        {
            try
            {
                CloseOSK();
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
            }
        }
        private void CloseOSK()
        {
            Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
            foreach (Process proc in process)
            {
                proc.CloseMainWindow();    //kill the process
            }
        }
        private void CloseTabTip()
        {
            try
            {
                Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
                foreach (Process proc in process)
                {
                    proc.Kill();    //kill the process before closing usercontrol
                }
            }
            catch (Exception)
            { }
        }

        private void GrdMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            CloseOSK();
        }

        private void txtName_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();

        }

        private void txtPassword_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();

        }
    }
}
