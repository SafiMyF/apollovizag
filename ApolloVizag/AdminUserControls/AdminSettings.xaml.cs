﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloVizag.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminSettings.xaml
    /// </summary>
    public partial class AdminSettings : UserControl
    {
        public event EventHandler EvntLogoutSettings;
        public AdminSettings()
        {
            InitializeComponent();
        }
        private void btnLogout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntLogoutSettings(this, null);
        }

        private void btnShowDesktop_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer");
        }

        private void btnRestart_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (MessageBox.Show("Are you sure want to restart the system.", "DRL", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /f /t 0");
            }
        }

        private void btnShutdown_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (MessageBox.Show("Are you sure want to shutdown the system.", "DRL", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");
            }
        }
    }
}
