﻿using ApolloVizag.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ApolloVizag
{
    public class Utilities
    {
        public static string SlideShowServerUrl
        {
            get { return ConfigurationManager.AppSettings["SlideShowserverUrl"]; }
        }

        public static string SlideShowHostedService
        {
            get { return ConfigurationManager.AppSettings["SlideShowHostedService"]; }
        }
        public static string UserName
        {
            get { return ConfigurationManager.AppSettings["UserName"]; }
        }

        public static string Password
        {
            get { return ConfigurationManager.AppSettings["Password"]; }
        }

        public static T DeserializeGeneric<T>(string serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return default(T);
                }
                if (serializedObject.Length == 0)
                {
                    if (default(T) != null)
                    {
                        return default(T);
                    }
                    return Activator.CreateInstance<T>();
                }
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (StringReader reader = new StringReader(serializedObject))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string SerializeObject<T>(T serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return string.Empty;
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    System.Xml.Serialization.XmlSerializerNamespaces xns = new System.Xml.Serialization.XmlSerializerNamespaces();
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(serializedObject.GetType());
                    xns.Add(string.Empty, string.Empty);
                    x.Serialize(ms, serializedObject, xns);
                    ms.Position = 0;
                    var reader = new StreamReader(ms);
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Departments GetDepartments(string strInfo)
        {
            Departments dept = DeserializeGeneric<Departments>(strInfo);
            return dept;
        }

    }
}
