﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace ApolloVizag.Model
{
	[XmlRoot(ElementName="Doctor")]
	public class Doctor {
		[XmlElement(ElementName="FirstName")]
		public string FirstName { get; set; }
		[XmlElement(ElementName="LastName")]
		public string LastName { get; set; }
		[XmlElement(ElementName="Gender")]
		public string Gender { get; set; }
		[XmlElement(ElementName="DOB")]
		public string DOB { get; set; }
		[XmlElement(ElementName="Email")]
		public string Email { get; set; }
		[XmlElement(ElementName="PhoneNumber")]
		public string PhoneNumber { get; set; }
		[XmlElement(ElementName="MobileNumber")]
		public string MobileNumber { get; set; }
		[XmlElement(ElementName="Qualification")]
		public string Qualification { get; set; }
		[XmlElement(ElementName="Address")]
		public string Address { get; set; }
		[XmlElement(ElementName="City")]
		public string City { get; set; }
		[XmlElement(ElementName="State")]
		public string State { get; set; }
		[XmlElement(ElementName="Country")]
		public string Country { get; set; }
		[XmlElement(ElementName="Pincode")]
		public string Pincode { get; set; }
		[XmlElement(ElementName="Speciality")]
		public List<string> Speciality { get; set; }
		[XmlElement(ElementName="SuperSpeciality")]
		public string SuperSpeciality { get; set; }
		[XmlElement(ElementName="Interests")]
		public string Interests { get; set; }
		[XmlElement(ElementName="PhotoUrl")]
		public string PhotoUrl { get; set; }
		[XmlElement(ElementName="Languages")]
		public string Languages { get; set; }
		[XmlElement(ElementName="Registration_Number")]
		public string Registration_Number { get; set; }
		[XmlElement(ElementName="Awards")]
		public string Awards { get; set; }
		[XmlElement(ElementName="Research")]
		public string Research { get; set; }
		[XmlElement(ElementName="Certifications")]
		public string Certifications { get; set; }
		[XmlElement(ElementName="Experience")]
		public string Experience { get; set; }
		[XmlElement(ElementName="Detailed_work_Experience")]
		public string Detailed_work_Experience { get; set; }
		[XmlElement(ElementName="NotInterestedIneDoc")]
		public string NotInterestedIneDoc { get; set; }
		[XmlElement(ElementName="Tariff")]
		public string Tariff { get; set; }
		[XmlElement(ElementName="Hospital")]
		public string Hospital { get; set; }
	}

	[XmlRoot(ElementName="Doctors")]
	public class Doctors {
		[XmlElement(ElementName="Doctor")]
		public List<Doctor> Doctor { get; set; }
	}

}
