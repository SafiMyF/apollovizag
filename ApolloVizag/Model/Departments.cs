﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace ApolloVizag.Model
{
    [XmlRoot(ElementName = "Department")]
    public class Department
    {
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "Departments")]
    public class Departments
    {
        [XmlElement(ElementName = "Department")]
        public List<Department> Department { get; set; }
    }


}